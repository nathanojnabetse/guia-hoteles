$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 1000  
    });
    $('#contacto').on('show.bs.modal', function (e){
      console.log("el modal se está mostrando")
      $('#contacoBTN').removeClass('btn-ouyline-success');
      $('#contacoBTN').addClass('btn-primary');
      $('#contacoBTN').prop('disabled',true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
      console.log("el modal contacto se mostro")
    });
    $('#contacto').on('hide.bs.modal', function (e){
      console.log("el modal se oculta")
    });
    $('#contacto').on('hidden.bs.modal', function (e){
      console.log("el modal contacto se oculto")
      $('#contacoBTN').prop('disabled',false);
    });
  });